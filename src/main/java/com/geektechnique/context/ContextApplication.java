package com.geektechnique.context;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ContextApplication {

    public static void main(String[] args) {

        System.setProperty("server.servlet.context-path", "/anothertest");

        SpringApplication.run(ContextApplication.class, args);
    }

}
